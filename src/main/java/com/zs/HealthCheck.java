package com.zs;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagement;
import com.amazonaws.services.simplesystemsmanagement.AWSSimpleSystemsManagementClientBuilder;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersRequest;
import com.amazonaws.services.simplesystemsmanagement.model.GetParametersResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import software.amazon.awscdk.services.ssm.StringParameter;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.stream.Stream;

public class HealthCheck implements RequestStreamHandler {

//    public static void main (String args [] ) throws IOException {
//
//        callAPI("https://rcs-shrd.demo.zsservices.com/connect"); // use array of strings
//
//
//    }



    static String portNo = "";
    static String connect =  "";
    static String webLink = "";
    static String paramStore = "";

    @Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws IOException  {

        JSONObject responseJson = new JSONObject();
        JSONParser parser = new JSONParser();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));


//
//        String urlStringStg = System.getenv("serverNameStg");
//        String urlStringProd = System.getenv("serverNameProd");

        String urlString = System.getenv("serverName");

        portNo = System.getenv("portNo");
        connect = System.getenv("connect");
        webLink = System.getenv("webLink");
        paramStore = System.getenv("paramStore");


        System.out.println(portNo+" " +connect + " " + webLink);
//
//        String[] splitArrayStg = urlStringStg.split(",");
//        String[] splitArrayProd = urlStringProd.split(",");

        String[] splitArray = urlString.split(",");

        String response = "";

        JSONObject event = null, params=null,queryString=null,header_auth = null;

//
//        try {
//            event = (JSONObject)parser.parse(reader); //.parse(reader);
//            System.out.println("event taken"+event.toJSONString());
//            System.out.println("event"+event.toString());
//        } catch (ParseException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }


//        params = (JSONObject) event.get("params");
//        System.out.println("params"+params);
//
//        queryString = (JSONObject) params.get("querystring");
//        System.out.println("queryString"+queryString);

        HashMap<String, String> map = new HashMap<>();
        String pass = parameterStore();

        map = callAPI(splitArray, pass,webLink);

     //   System.out.println(response);

        int c = 0;

       // System.out.println(map.size());


        for (Map.Entry<String, String> entry : map.entrySet()) {


            if ( entry.getValue()  == "Success"){


//                System.out.println("inside Hasp loop");

                c = c+1;
  //              System.out.println("c"+c);
                if (c ==  map.size()){
                    response = Constants.SUCCESS;
                    break;
                }
                response = response + entry.getKey() + " " + Constants.PASS + "\n" ;
            }else {
                    //System.out.println(entry.getValue());

                    response = response + entry.getKey() + " " + Constants.FAIL + "\n" ;


            }
        }

       // System.out.println("c"+c);

        try {
            responseJson.put("Response",response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("response"+response);

        OutputStreamWriter writer = new OutputStreamWriter(output, "UTF-8");
        writer.write(responseJson.toJSONString());
        writer.close();

    }

    private static HashMap<String, String> callAPI( String [] urlString, String pass, String wenUrl) throws IOException {

        HashMap<String, String> map = new HashMap<>();


        URL webUrl1 = new URL(wenUrl);
//            URL url = new URL("https://rcs-shrd.demo.zsservices.com/connect");

        URLConnection connection1 = webUrl1.openConnection();

        HttpURLConnection httpConn1 = (HttpURLConnection) connection1;
        httpConn1.setRequestProperty("Authorization", "Key " + pass);
        httpConn1.setRequestMethod("GET");

        int codeWebUrl = 0 ;
        try {
            codeWebUrl = httpConn1.getResponseCode();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        System.out.println("web url status code" + codeWebUrl);

        if (codeWebUrl == 200 ) {

            map.put(wenUrl, Constants.SUCCESS);
            System.out.println("web url login success");

        }else {
            map.put(wenUrl, Constants.FAIL);
            System.out.println("web url login fail");
        }

            for (int i = 0; i < urlString.length; i++) {

                CookieManager cookieManager = new CookieManager();
                CookieHandler.setDefault(cookieManager);

                String finalUrl = Constants.HTTPText + "://" + urlString[i] + ":" + portNo + connect; //values to be kept in Environment variable

                System.out.println(finalUrl);

                URL url = new URL(finalUrl);
//            URL url = new URL("https://rcs-shrd.demo.zsservices.com/connect");

//                URLConnection connection = null ;

                HttpURLConnection connection = null;
                int code = 0;
                try {
                    connection = (HttpURLConnection) (url).openConnection();
                    connection.setConnectTimeout(10000);
                    connection.setReadTimeout(10000);
                    connection.setRequestMethod("GET");
                    connection.setRequestProperty("Authorization", "Key " + pass);
                    connection.connect();
                    code = connection.getResponseCode();
                } catch (Exception e) {
//                    e.printStackTrace();
                    System.out.println(e.getMessage());
                }


//                HttpURLConnection httpConn = (HttpURLConnection) connection;
//                httpConn.setRequestProperty("Authorization", "Key " + pass);
//                httpConn.setConnectTimeout(1000*10);
//                httpConn.setRequestMethod("GET");
//
//
//                try {
//                  //  code = httpConn.getResponseCode();
//
//                   System.out.println("Respose Code "+ code);
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
                System.out.println("Respose Code "+ code);

                List<HttpCookie> cookies = null;
                ArrayList<String> cookiesName = new ArrayList<>();

                if (code == 200) {  //

                    System.out.println("Checking cookies");

                    connection.getContent();
                    cookies = cookieManager.getCookieStore().getCookies();

                    for (HttpCookie cookie : cookies) {
                        cookiesName.add(cookie.getName());
                    }

//                if (cookiesName.contains("rscid-legacy") && cookiesName.contains("rscid")) {

                    if (cookiesName.contains("rscid")) {
//                    System.out.println(Constants.SUCCESS);
                        // respone = Constants.SUCCESS;
                        map.put(urlString[i], Constants.SUCCESS);
                        System.out.println(urlString[i] + " " + Constants.SUCCESS);
                    } else {
//                    responseArray.add(Constants.FAIL);
                        map.put(urlString[i], Constants.FAIL);
                        System.out.println(urlString[i] + " " + Constants.FAIL);
                    }
                } else {
                    map.put(urlString[i], Constants.FAIL);
//                responseArray.add(Constants.FAIL);
                    System.out.println(urlString[i] + " " + Constants.FAIL);
                }

            }


        return map;
    }
        // dono mai check karo, display fail -> log fail proper

        //  Checking node - nodeName - FAIL|PASS break ni karna
                
        //checking nodeName fail -> konsa fail konsa pass, next line mai for second node..

//        return respone;



    private static  String  parameterStore() {

        String pass = "";
        String nameofCurrMethod = new Throwable().getStackTrace()[0].getMethodName();
        try {

            AWSSimpleSystemsManagement client= AWSSimpleSystemsManagementClientBuilder.defaultClient();
            GetParametersRequest request= new GetParametersRequest();
            request.withNames(paramStore)
                    .setWithDecryption(true);

            //name to changed as per parameter value
            GetParametersResult result = client.getParameters(request);
            pass = result.getParameters().get(0).getValue();
            System.out.println(pass);

        }
        catch (Exception e) {
            System.out.println(e.getMessage()+ " " + nameofCurrMethod + " ");
        }

        return  pass;

    }

}
