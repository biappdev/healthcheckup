package com.zs;

public class Constants {

    private Constants(){}

    final static String PageTitle = "RConnect Health Check";
    final static String FAIL = "Fail";
    final static String SUCCESS = "Success";
    final static String URL = "https://rcs-shrd.demo.zsservices.com/connect";
    final static String HTTPText = "http";
    final static String PASS = "Pass";
}
